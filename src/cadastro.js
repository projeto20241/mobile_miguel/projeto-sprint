import React, { useState } from "react";
import { StatusBar } from "expo-status-bar";
import { TextInput, StyleSheet, Text, View, ScrollView, TouchableOpacity, Image,} from "react-native";
import logoBV from '../assets/logoBV.png'
import { useNavigation } from '@react-navigation/native'; // Importando o hook useNavigation

export default function Cadastro() {
  const navigation = useNavigation(); // Obtendo a instância de navegação

  const [email, setEmail] = useState('');
  const [nome, setNome] = useState('');
  const [ap, setAp] = useState('');
  const [senha, setSenha] = useState('');
  const [telefone, setTelefone] = useState('');
  const [confirmarSenha, setConfirmarSenha] = useState('');
  const [errorMessage, setErrorMessage] = useState('');

  const handleCadastro = () => {
    if (!email || !nome || !ap || !senha || !confirmarSenha || !telefone) {
      setErrorMessage('Todos os campos devem ser preenchidos.');
    } else if (senha !== confirmarSenha) {
      setErrorMessage('As senhas não coincidem. Por favor, verifique.');
    } else {
      navigation.navigate('Home');
    }
  };//cadastro

  return (
    <View style={styles.container}>
      
      <Image source={logoBV} style={styles.imagem} />
      <View style={styles.inputsContainer}>
        <TextInput
          style={styles.input}
          placeholder="Email"
          onChangeText={setEmail}
          value={email}
        />{/* input email */}
        <TextInput
          style={styles.input}
          placeholder="Nome"
          onChangeText={setNome}
          value={nome}
        />{/* input Nome */}
        <TextInput
          style={styles.input}
          placeholder="AP"
          onChangeText={setAp}
          value={ap}
        />{/* input AP */}
                <TextInput
          style={styles.input}
          placeholder="Telefone"
          onChangeText={setTelefone}
          value={ap}
        />{/* input AP */}
        <TextInput
          style={styles.input}
          placeholder="Senha"
          secureTextEntry={true}
          onChangeText={setSenha}
          value={senha}
        />{/* input senha */}
        <TextInput
          style={styles.input}
          placeholder="Confirmar Senha"
          secureTextEntry={true}
          onChangeText={setConfirmarSenha}
          value={confirmarSenha}
        />{/* input Confirma Senha */}
      </View>
      {errorMessage ? <Text style={styles.errorMessage}>{errorMessage}</Text> : null}
      <View style={styles.buttonContainer}>
        <TouchableOpacity style={styles.button} onPress={handleCadastro}>
          <Text style={styles.buttonText}>Cadastrar</Text>
         </TouchableOpacity>{/* botão cadastrar */}
      </View>
      <StatusBar style="false" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#efd5c4",
  },
  inputsContainer: {
    width: "80%",
    marginBottom: 5,
  },
  input: {
    height: 40,
    borderWidth: 1,
    borderColor: "black",
    borderRadius: 20,
    paddingHorizontal: 10,
    paddingLeft: 10,
    backgroundColor: "#dbc4b6",
    marginBottom: 5,
    opacity: 0.5
  },
  buttonContainer: {
    width: "60%",
    marginBottom: 100,
    marginTop: 20,
  },
  button: {
    backgroundColor: "#dbc4b6",
    borderRadius: 10,
    padding: 10,
    marginBottom: 4,
    alignItems: "center",
    borderColor: "black",
    borderWidth: 1,
  },
  buttonText: {
    color: "black",
    fontWeight: "bold",
  },
  imagem: {
    height: "40%",
    width: "100%",
    marginTop: -50,
    marginBottom: 40,
  },
  errorMessage: {
    color: 'black',
    marginBottom: 10,
  }
});
