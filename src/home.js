import { StatusBar } from "expo-status-bar";
import { StyleSheet, Text, View, ScrollView } from "react-native";
import { useNavigation } from '@react-navigation/native';

export default function Home() {

  const navigation = useNavigation(); 

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Text style={styles.text}>MENU</Text>
      </View>
      <View style={styles.content}>
        <ScrollView>
          <Text style={styles.text}>CONTEUDO</Text>
        </ScrollView>
      </View>
      <View style={styles.footer}>
        <Text style={styles.text}>RODAPE</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "space-between",
  },
  header: {
    height: 50,
    backgroundColor: "#efd5c4",
  },

  content: {
    flex: 1,
    backgroundColor: "#efd5c4",
    opacity: 0.5
  },

  footer: {
    height: 50,
    backgroundColor: "#efd5c4",
  },
  text: {
    fontSize: 16,
    fontWeight: "bold",
    textAlign: "center",
  },
});

