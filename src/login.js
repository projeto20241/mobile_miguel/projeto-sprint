import React, { useState } from "react";
import { StatusBar } from "expo-status-bar";
import { TextInput, StyleSheet, Text, View, TouchableOpacity, Image } from "react-native";
import logoBV from '../assets/logoBV.png';
import { useNavigation } from '@react-navigation/native';
import { Feather } from '@expo/vector-icons';

export default function Login() {
  const navigation = useNavigation();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [errorMessage, setErrorMessage] = useState('');
  const [showPassword, setShowPassword] = useState(false);
  const [eyeIcon, setEyeIcon] = useState("eye");

  const handleLogin = () => {
    if (email === 'M' && password === '1') {
      navigation.navigate('Home');
      setEmail('');
      setPassword('');
    } else {
      setErrorMessage('Credenciais inválidas. Por favor, tente novamente.');
    }
  };

  const mostrarSenha = () => {
    setShowPassword(!showPassword);
    setEyeIcon(showPassword ? "eye" : "eye-off");
  };

  return (
    <View style={styles.container}>
      <Image source={logoBV} style={styles.imagem} />
      <View style={styles.inputsContainer}>
        <TextInput
          style={styles.input}
          placeholder="Email"
          onChangeText={setEmail}
          value={email}
        />
        <View style={styles.passwordInputContainer}>
          <TextInput
            style={styles.passwordInput}
            placeholder="Senha"
            secureTextEntry={!showPassword}
            onChangeText={setPassword}
            value={password}
            onFocus={() => setEyeIcon("eye")}
          />
          {password !== '' && (
            <TouchableOpacity onPress={mostrarSenha} style={styles.eyeIcon}>
              <Feather name={eyeIcon} size={24} color="black" />
            </TouchableOpacity>
          )}
        </View>
      </View>
      {errorMessage ? <Text style={styles.errorMessage}>{errorMessage}</Text> : null}
      <View style={styles.buttonContainer}>
        <TouchableOpacity style={styles.button} onPress={handleLogin}>
          <Text style={styles.buttonText}>Entrar</Text>
        </TouchableOpacity>
      </View>
      <StatusBar style="false" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#efd5c4",
  },
  inputsContainer: {
    width: "80%",
    marginBottom: 5,
  },
  input: {
    height: 40,
    borderWidth: 1,
    borderColor: "black",
    borderRadius: 20,
    paddingHorizontal: 10,
    paddingLeft: 10,
    backgroundColor: "#dbc4b6",
    marginBottom: 5,
    opacity: 0.5
  },
  passwordInputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    width: "100%", 
  },
  passwordInput: {
    flex: 1,
    height: 40,
    borderWidth: 1,
    borderColor: "black",
    borderRadius: 20,
    paddingLeft: 10, 
    backgroundColor: "#dbc4b6",
    marginBottom: 5,
    opacity: 0.5,
  },
  eyeIcon: {
    position: 'absolute', 
    right: 10,
    top: 10, 
  },
  buttonContainer: {
    width: "60%",
    marginBottom: 100,
    marginTop: 20,
  },
  button: {
    backgroundColor: "#dbc4b6",
    borderRadius: 10,
    padding: 10,
    marginBottom: 4,
    alignItems: "center",
    borderColor: "black",
    borderWidth: 1,
  },
  buttonText: {
    color: "black",
    fontWeight: "bold",
  },
  imagem: {
    height: "40%",
    width: "100%",
    marginTop: -50,
    marginBottom: 40,
  },
  errorMessage: {
    color: 'red',
    marginBottom: 10,
  }
});
